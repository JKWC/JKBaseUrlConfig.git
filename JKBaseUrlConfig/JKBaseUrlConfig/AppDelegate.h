//
//  AppDelegate.h
//  JKBaseUrlConfig
//
//  Created by 王冲 on 2019/1/18.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

