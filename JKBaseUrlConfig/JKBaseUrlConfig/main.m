//
//  main.m
//  JKBaseUrlConfig
//
//  Created by 王冲 on 2019/1/18.
//  Copyright © 2019年 JK科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
